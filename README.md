These libraries are used by [Konstruktiv Berlin](https://www.konstruktiv-berlin.de). They are mainly based on the official Kicad libraries early 2019 with some extra footprints created by Konstruktiv. 

This was done to:
* have full control over changes
* to have a common library to share with clients and collaborators
* to fit them into Git repos of Kicad projects as submodules

Main differences to the standard libs:
* to be distiguishable from the standard libs the suffix _k was added to folders and files
* inside footprints the path to 3D parts was altered to be relative to the project so they would be found when used as git submodule