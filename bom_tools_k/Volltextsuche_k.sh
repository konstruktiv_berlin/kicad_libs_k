#!/bin/bash
#
# Skript zur Volltextsuche im kiCAD Ordner
# Felix Piela
# Jan 2017
#
echo "bitte Suchbegriff eingeben"
#echo "bitte *, ? als Platzhalter verwenden (z.B. *STM32F4*)"
read string
echo "der folgende Suchbegriff wurde gelesen:"
echo "$string"
echo "damit wird in /usr/share/kicad/library gesucht"
grep -r "$string" /usr/share/kicad/library/
echo "jetzt wird in den Konstruktiv Bibliotheken gesucht:"
grep -r "$string" ./
echo "mehr konnte nicht gefunden werden"
